<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cwp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{F)%AC<oSR+cM/N5h:3LU3nH*cQY[_]:>:2L6!sZOWV-d8?&~k0vMGhap `![A)%' );
define( 'SECURE_AUTH_KEY',  '=s#[AWjy[lt,C9<dUrr[0Z/IQ/gy%|o&y7Qn4zY,8v{sY#c3$l@X4h{Dp!9XF$LS' );
define( 'LOGGED_IN_KEY',    '7I~Au_o?cMwy{5dYMq#OtYbaViN*hls5P^^?ECW,!8vF^u@BDelD3Y6YH^-w0C6b' );
define( 'NONCE_KEY',        'gE<6SE?Z kt$0Px||0O=xWMCS9r@-L:ppDt!.PBE%mT]G*yq`8+In=8)lm*Z&6fT' );
define( 'AUTH_SALT',        'RGHXJVzLm<o/lTEk7W|pK?}pR}}a3=z!BYhtI}mJOM4emfSZ. aAf)%d2BOt%fDH' );
define( 'SECURE_AUTH_SALT', 'n}{?Z-?%)_gmq;v|4h7RemU}TP[>!Z|N{6uNin9?2SA=-nC! K-Zmg(/6w@AP $f' );
define( 'LOGGED_IN_SALT',   'M+}Y+^a%(R$@WL2bbDt<,Y5Q|lAR;W<LJ9C%E?EJ8e]&K4,12=c5{u7ezx!;)X5]' );
define( 'NONCE_SALT',       'gT<8#&bz ;R~Rdbu8xS~,it`K0&p|@GHAICpxNWj.O_cvPF*mwiJi.|fT@~WZq6^' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
