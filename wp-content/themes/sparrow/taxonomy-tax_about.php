<?php
/*

*/

; ?>

<?php get_header( 'page' ); ?>

<!-- Page Title
================================================== -->
<div id="page-title">

    <div class="row">

        <div class="ten columns centered text-center">
            <h1>Single taxonomy About Post<span>.</span></h1>

            <p>Aenean condimentum, lacus sit amet luctus lobortis, dolores et quas molestias excepturi
                enim tellus ultrices elit, amet consequat enim elit noneas sit amet luctu. </p>
        </div>

    </div>

</div> <!-- Page Title End-->

<!-- Content
================================================== -->
<div class="content-outer">

    <div id="page-content" class="row">

        <div id="primary" class="eight columns">

            <article class="post">

                <div class="entry-header cf">

					<?php
					// проверяем есть ли посты в глобальном запросе - переменная $wp_query
					if ( have_posts() ){
					// перебираем все имеющиеся посты и выводим их
					while ( have_posts() ){
					the_post();
					?>

                    <h1><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?>.</a></h1>
                    <p class="post-meta">

                        <!--                        <time class="date" datetime="2014-01-14T11:24">-->
						<?php //the_time( 'F jS, Y' );
						?><!--</time>-->

                        <span class="categories">
                          <?php the_field( 'about-data' ); ?>
							<?php the_terms( get_the_ID(), 'tax_about', '<a href="#">', '</a> / ' ); ?>
                     </span>

                    </p>
                </div>

                <div class="post-thumb">
                    <a href="<?php the_permalink(); ?>" title=""><img src="<?php the_field( 'about-photo' ); ?>" alt=""></a>
                </div>
                <div class="post-thumb">
                    <a href="<?php the_permalink(); ?>" title=""><?php the_post_thumbnail(); ?></a>
                </div>

                <div class="post-content">
                    <p><?php the_content(); ?></p>
                </div>
				<?php
				}
				?>

                <div class="navigation">
                    <div class="next-posts"><?php next_posts_link(); ?></div>
                    <div class="prev-posts"><?php previous_posts_link(); ?></div>
                </div>

				<?php
				}
				// постов нет
				else {
					echo "<h2>Записей нет.</h2>";
				}
				?>


            </article> <!-- post end -->


        </div> <!-- Primary End-->

        <div id="secondary" class="four columns end">
			<?php get_sidebar(); ?>
        </div> <!-- Secondary End-->

    </div>

</div> <!-- Content End-->

<!-- Tweets Section
================================================== -->
<section id="tweets">

    <div class="row">

        <div class="tweeter-icon align-center">
            <i class="fa fa-twitter"></i>
        </div>

        <ul id="twitter" class="align-center">
            <li>
               <span>
               This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
               Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
               <a href="#">http://t.co/CGIrdxIlI3</a>
               </span>
                <b><a href="#">2 Days Ago</a></b>
            </li>
            <!--
			<li>
			   <span>
			   This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
			   Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
			   <a href="#">http://t.co/CGIrdxIlI3</a>
			   </span>
			   <b><a href="#">3 Days Ago</a></b>
			</li>
			-->
        </ul>

        <p class="align-center"><a href="#" class="button">Follow us</a></p>

    </div>

</section> <!-- Tweets Section End-->

<?php get_footer(); ?>
