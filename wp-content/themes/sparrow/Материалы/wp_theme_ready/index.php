<?php get_header(); ?>
    <main>
      <h1 class="visually-hidden"> CofeePlanet's site </h1>
      <div class="first-screen">
        <div class="wrapper">
          <div class="slogan">
            <?php if( is_active_sidebar('first-screen') ): ?>
            <p class="slogan__text">
              <?php dynamic_sidebar('first-screen'); ?>
            </p>
            <?php endif; ?>
            <p class="slogan__btns">
              <a href="#book" class="btn slogan__btn btn_modal">Book a table</a>
              <a href="#video" class="btn slogan__btn slogan__btn_white btn_modal">Watch video</a>
            </p>
          </div>
        </div>
      </div>
      <section id="menu" class="wrapper product-range">
        <h2 class="product-range__h cofee-planet__h"> Make a choice </h2>
      <?php if(is_active_sidebar('choice')): ?>
        <p class="product-range__text">
          <?php dynamic_sidebar('choice'); ?>
        </p>
      <?php endif; ?>
      <?php
        $terms = get_terms([
            'taxonomy' => 'product-category',
            'hide_empty' => true
        ]);     
      ?>
        <ul class="product-tab">
        <?php foreach($terms as $key => $term):
                if($key === 0):
        ?>
          <li class="product-tab__li active">
        <?php else: ?>
          <li class="product-tab__li">
        <?php endif; ?>
            <a href="#<?php echo $term->slug; ?>" class="product-tab__link">
              <?php echo $term->name; ?>
            </a>
          </li>
        <?php endforeach; ?>
        </ul>
        <ul class="product-content">
        <?php foreach($terms as $key => $term):
                if($key === 0):
        ?>
          <li id="<?php echo $term->slug; ?>" class="product-content__li active">
        <?php else: ?>
          <li id="<?php echo $term->slug; ?>" class="product-content__li">
          <?php endif; ?>
            <ul class="row">
            <?php
              $products_query = new WP_Query([
                'post_type' => 'products',
                'product-category' => $term->slug
              ]);
              $products = $products_query->posts;
              foreach($products as $prod):
                $title = $prod->post_title;
                $img = get_the_post_thumbnail_url( $prod, 'full' );
                $price = get_fields($prod->ID)['product-price'];
            ?>
              <li class="product">
                <img src="<?php echo $img; ?>" alt="Photo of product" class="product__img">
                <h3 class="product__name">
                  <?php echo $title; ?>
                </h3>
                <p class="product__price">
                  <?php echo $price; ?>
                </p>
              </li>
            <?php endforeach; ?>
            </ul>
          </li>
        <?php endforeach; ?>
        </ul>
      </section>
      <?php
        if(is_active_sidebar('article')){
          dynamic_sidebar('article');
        }
      ?>
      <section id="features" class="features wrapper">
        <h2 class="features__h cofee-planet__h"> Our features </h2>
      <?php if(is_active_sidebar('features')): ?>
        <p class="features__p">
          <?php dynamic_sidebar('features'); ?>
      <?php endif; ?>
        </p>
        <ul class="features-list row">
        <?php  
          $features = get_posts([

            'post_type' => 'features',
            'orderby'   => 'date',
            'order'     => 'DESC',
            'numberposts' => -1
          ]);
          foreach($features as $feature):
            $HTMLClass = get_fields($feature->ID)['features-class'];
            $title = $feature->post_title;
        ?>
          <li class="features-list__li <?php echo $HTMLClass; ?>">
            <?php echo $title; ?>
          </li>
        <?php endforeach; ?>
        </ul>
      </section>
      <section class="feedback-container wrapper">
        <h2 class="cofee-planet__h feedback-container__h"> Our happy visitors say: </h2>
        <ul class="feedbacks row">
        <?php 
          $reviews = get_posts([
            'post_type' => 'reviews',
            'orderby'   => 'date',
            'order'     => 'DESC',
            'numberposts' => -1
          ]);
          foreach($reviews as $review):
            $name = $feature->post_title;
            $img = get_the_post_thumbnail_url( $review->ID, 'full' );
            $text = $review->post_content;
            $date = get_the_date( 'd.m.Y', $review );
            $datetime = get_the_date( 'Y-m-d', $review );
        ?>
          <li class="feedback">
            <figure class="feedback__mask">
              <img src="<?php echo $img; ?>" alt="Photo" class="feedback__img">
              <figcaption class="feedback__name">
                <?php echo $name; ?>
              </figcaption>
            </figure>
            <p class="feedback__text">
              <?php echo $text; ?>
            </p>
            <p class="feedback__data">
              <time class="feedback__date" datetime="<?php echo $datetime; ?>">
                <?php echo $date; ?>
              </time>
            </p>
          </li>
        <?php endforeach; ?>
        </ul>
      </section>
      <section id="contacts" class="contacts wrapper">
        <h2 class="visually-hidden">CofeePlanet contacts</h2>
        <section class="contacts-info">
          <h3 class="cofee-planet__h contacts-info__h"> Come to us! </h3>
        <?php 
          if(is_active_sidebar('contacts')){ 
            dynamic_sidebar('contacts');
          }
        ?>
        </section>
        <div class="contacts-info__map">
          <a class="contacts-info__map_link" href="https://www.google.com.ua/maps/place/City.com/@50.4907126,30.4848155,14z/data=!4m5!3m4!1s0x40d4cdf957eaff13:0x5a439dfd376d0ad8!8m2!3d50.4900462!4d30.4954129?hl=ru" rel="nofollow noopener" target="_blank">
            <img src="<?php echo train_get_image('map.jpg'); ?>" alt="Map" class="contacts-info__img">
            <span class="visually-hidden">Google maps link</span>
          </a>
        <?php if(is_active_sidebar('map-block')): ?>
          <div id="map" class="contacts-info__map_container">
            <?php dynamic_sidebar('map-block'); ?>
          </div>
        <?php endif; ?>
        </div>
      </section>
    </main>

<?php get_footer(); ?>