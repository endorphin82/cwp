    <footer class="footer wrapper"> Copyright <?php echo date("Y");  ?> CoffeePlanet </footer>
    <div class="modal-container d-n">
      <div class="wrapper modal-container_main">
        <button class="closer">
          <span class="visually-hidden">Close</span>
        </button>
        <section id="book" class="modal modal-book">
          <h2 class="modal-book__h cofee-planet__h"> Book a table </h2>
          <p class="modal-book__p"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor </p>
          <form action="<?php echo esc_url( admin_url('admin-ajax.php') ); ?>" class="modal-book__form" method="POST">
            <input type="hidden" name="action" value="new_order">
            <input type="text" class="modal-book__input" placeholder="Name" name="order-name">
            <input type="text" class="modal-book__input" placeholder="Phone" name="order-phone">
            <button class="btn modal-book__btn" type="submit">Send</button>
            <script>
              window.addEventListener('load', function(){
                var form = $('.modal-book__form');
                form.on('submit', function(e){
                  e.preventDefault();
                  var url = $(this).attr('action');
                  var data = form.serializeArray();
                  $.post(
                    url,
                    data,
                    function(res){
                      form.html(res);
                    }
                  );
                });
              });
            </script>
          </form>
        </section>
        <section id="video" class="modal modal-video">
          <h2 class="visually-hidden">Watch a video about CafeePlanet</h2>
        <?php if(is_active_sidebar('video')): ?>
          <div class="modal-video__video">
            <?php dynamic_sidebar('video') ?>
          </div>
        <?php endif; ?>
        </section>
      </div>
    </div>
    <?php wp_footer(); ?>
  </body>
</html>