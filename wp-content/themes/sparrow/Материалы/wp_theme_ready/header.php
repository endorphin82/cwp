<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv='X-UA-Compatible' content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <header class="header">
      <nav class="navigation wrapper">
        <?php the_custom_logo(); ?>
        <?php 
              $locations = get_nav_menu_locations();
              $menu_header_id = $locations['menu-header'];
              $args = array(
                'order'                  => 'ASC',
                'orderby'                => 'menu_order',
                'output'                 => ARRAY_A,
                'output_key'             => 'menu_order',
                'update_post_term_cache' => false,
              );
              $menu_header = wp_get_nav_menu_items($menu_header_id, $args);
        ?>
        <ul class="navigation__ul">
        <?php 
          foreach( $menu_header as $item ):
        ?>
          <li class="navigation__li">
            <a href="<?php  echo $item->url; ?>" class="navigation__a"> 
              <?php echo $item->title; ?>    
            </a>
          </li>
        <?php endforeach;
              $menu_phone_id = $locations['menu-phone'];
              $menu_phone = wp_get_nav_menu_items( $menu_phone_id, $args );
              foreach( $menu_phone as $item ):
        ?>
          <li class="navigation__li navigation__li_tel">
            <a href="<?php echo $item->url; ?>" class="navigation__a"><?php echo $item->title; ?></a>
          </li>
        <?php endforeach; ?>
        </ul>
        <button class="mobile-menu"><span class="visually-hidden">Open mobile menu</span></button>
      </nav>
    </header>