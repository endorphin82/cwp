<?php

include_once(__DIR__.'/inc/train_simple_text.php');
include_once(__DIR__.'/inc/train_iframe.php');
include_once(__DIR__.'/inc/train_phone.php');
include_once(__DIR__.'/inc/train_mail.php');
include_once(__DIR__.'/inc/train_time.php');
include_once(__DIR__.'/inc/train_address.php');
include_once(__DIR__.'/inc/train_article.php');


add_action('wp_enqueue_scripts', 'train_scripts');
add_action('after_setup_theme', 'train_setup');
add_action('widgets_init', 'train_widgets');
add_action('init', 'train_registration');
add_action('manage_posts_custom_column', 'train_echo_column',5,2);

add_action('wp_ajax_new_order', 'train_order');
add_action('wp_ajax_nopriv_new_order', 'train_order');


add_filter('show_admin_bar', '__return_false');
add_filter('manage_posts_columns', 'train_add_column');

function train_scripts(){
	wp_enqueue_style('styles', get_template_directory_uri() . '/assets/css/styles.css', [], '1.0.0', 'all');

	wp_enqueue_script('my-jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', [], '2.2', true);

	wp_enqueue_script('slick', get_template_directory_uri() . '/assets/js/slick.min.js', ['my-jquery'], '2.2', true);

	wp_enqueue_script('js', get_template_directory_uri() . '/assets/js/js.js', ['my-jquery', 'slick'], '2.2', true);
}


function train_setup(){
	add_theme_support('custom-logo');
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');

	register_nav_menu('menu-header', 'Menu in header');
	register_nav_menu('menu-phone', 'Menu for phone number');
}

function train_widgets(){
	register_sidebar([
		'name' => 'First screen',
		'id' => 'first-screen',
		'description' => 'Heading in first screen',
		'before_widget' => null,
		'after_widget' => null
	]);
	register_sidebar([
		'name' => 'Make a choice text',
		'id' => 'choice',
		'description' => 'Text in the \'Make a choice\' section',
		'before_widget' => null,
		'after_widget' => null
	]);
	register_sidebar([
		'name' => 'Main article block',
		'id' => 'article',
		'description' => 'Main article section',
		'before_widget' => null,
		'after_widget' => null
	]);
	register_sidebar([
		'name' => 'Features text',
		'id' => 'features',
		'description' => 'Text in the \'Features\' section',
		'before_widget' => null,
		'after_widget' => null
	]);
	register_sidebar([
		'name' => 'Contact us section',
		'id' => 'contacts',
		'description' => 'Sidebar for \'Contact us\' section',
		'before_widget' => null,
		'after_widget' => null
	]);
	register_sidebar([
		'name' => 'Map',
		'id' => 'map-block',
		'description' => 'Sidebar for \'Map\' section',
		'before_widget' => null,
		'after_widget' => null
	]);
	register_sidebar([
		'name' => 'Video pop-up',
		'id' => 'video',
		'description' => 'Sidebar for \'Watch video\' pop-up',
		'before_widget' => null,
		'after_widget' => null
	]);

	register_widget('train_text');
	register_widget('train_iframe');
	register_widget('train_phone');
	register_widget('train_mail');
	register_widget('train_time');
	register_widget('train_address');
	register_widget('train_article');
}


function train_registration(){
	register_post_type('main-article', [
		'labels' => [
			'name'               => 'Article', // основное название для типа записи
			'singular_name'      => 'Artecle', // название для одной записи этого типа
			'add_new'            => 'Add new articel', // для добавления новой записи
			'add_new_item'       => 'Add new article', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Update the articel', // для редактирования типа записи
			'new_item'           => 'A new article', // текст новой записи
			'view_item'          => 'See the articel', // для просмотра записи этого типа.
			'search_items'       => 'Search', // для поиска по этим типам записи
			'not_found'          => 'Nothing found', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Nothing in trash', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Articles', // название меню
		],
		'public'              => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-media-document', 
		'hierarchical'        => false,
		'supports'            => array('title', 'editor'),
		'has_archive' => false,
	]);

	register_post_type('products', [
		'labels' => [
			'name'               => 'Product', // основное название для типа записи
			'singular_name'      => 'Product', // название для одной записи этого типа
			'add_new'            => 'Add new product', // для добавления новой записи
			'add_new_item'       => 'Add new product', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Update the product', // для редактирования типа записи
			'new_item'           => 'A new product', // текст новой записи
			'view_item'          => 'See the product', // для просмотра записи этого типа.
			'search_items'       => 'Search', // для поиска по этим типам записи
			'not_found'          => 'Nothing found', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Nothing in trash', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Products', // название меню
		],
		'public'              => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-store', 
		'hierarchical'        => false,
		'supports'            => array('title', 'thumbnail'),
		'has_archive' => false,
		'taxonomies' => array('product-category')
	]);

	register_taxonomy('product-category', array('products'), array(
		'labels'                => array(
			'name'              => 'Product category',
			'singular_name'     => 'Category',
			'search_items'      => 'Find category',
			'all_items'         => 'All categories',
			'view_item '        => 'View category',
			'edit_item'         => 'Edit',
			'update_item'       => 'Update',
			'add_new_item'      => 'Add category',
			'new_item_name'     => 'Add category',
			'menu_name'         => 'All categories',
		),
		'description'           => '',
		'public'                => true,
		'hierarchical'          => true
	));

	register_post_type('features', [
		'labels' => [
			'name'               => 'Feature', // основное название для типа записи
			'singular_name'      => 'Feature', // название для одной записи этого типа
			'add_new'            => 'Add new feature', // для добавления новой записи
			'add_new_item'       => 'Add new feature', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Update the feature', // для редактирования типа записи
			'new_item'           => 'A new feature', // текст новой записи
			'view_item'          => 'See the feature', // для просмотра записи этого типа.
			'search_items'       => 'Search', // для поиска по этим типам записи
			'not_found'          => 'Nothing found', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Nothing in trash', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Features', // название меню
		],
		'public'              => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-info', 
		'hierarchical'        => false,
		'supports'            => array('title', 'thumbnail'),
		'has_archive' => false
	]);

	register_post_type('reviews', [
		'labels' => [
			'name'               => 'Review', // основное название для типа записи
			'singular_name'      => 'Review', // название для одной записи этого типа
			'add_new'            => 'Add new review', // для добавления новой записи
			'add_new_item'       => 'Add new review', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Update the review', // для редактирования типа записи
			'new_item'           => 'A new review', // текст новой записи
			'view_item'          => 'See the review', // для просмотра записи этого типа.
			'search_items'       => 'Search', // для поиска по этим типам записи
			'not_found'          => 'Nothing found', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Nothing in trash', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Reviews', // название меню
		],
		'public'              => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-format-chat', 
		'hierarchical'        => false,
		'supports'            => array('title', 'thumbnail', 'editor'),
		'has_archive' => false
	]);
	register_post_type('orders', [
		'labels' => [
			'name'               => 'Order', // основное название для типа записи
			'singular_name'      => 'Order', // название для одной записи этого типа
			'add_new'            => 'Add new order', // для добавления новой записи
			'add_new_item'       => 'Add new order', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Update the order', // для редактирования типа записи
			'new_item'           => 'A new order', // текст новой записи
			'view_item'          => 'See the order', // для просмотра записи этого типа.
			'search_items'       => 'Search', // для поиска по этим типам записи
			'not_found'          => 'Nothing found', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Nothing in trash', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Orders', // название меню
		],
		'public'              => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-universal-access', 
		'hierarchical'        => false,
		'supports'            => array('title'),
		'has_archive' => false
	]);
}

function train_order(){
	$name = wp_strip_all_tags($_POST['order-name']);
	$phone = wp_strip_all_tags($_POST['order-phone']);
	$id = wp_insert_post( wp_slash([
		'post_title' => 'Order from: '.$name,
		'post_type' => 'orders',
		'post_status' => 'publish',
		'meta_input' => [
			'order-name' => $name,
			'order-phone' => $phone
		]
	]) );
	if( $id ){
		update_field('order-name', $name, $id);
		update_field('order-phone', $phone, $id);
	}
	echo 'Thank you! We\'ve got your order. Your order\'s number is: <strong>#' . $id.'</strong>';
	wp_die();
}

function train_add_column($def){
	$def['order-id'] = 'Order ID';
	return $def;
}

function train_echo_column($name, $id){
	if( $name === 'order-id' ){
		echo '#'.$id;
	}
}


function train_get_image($name){
	return get_template_directory_uri() . '/assets/img/' . $name;
}


























?>