<?php

include_once(__DIR__.'/inc/train_simple_text.php');



wp_enqueue_style('styles', get_template_directory_uri() . '/assets/css/styles.css', [], '1.0.0', 'all');

wp_enqueue_script('js', get_template_directory_uri() . '/assets/js/js.js', ['my-jquery', 'slick'], '2.2', true);

register_sidebar([
	'name' => 'First screen',
	'id' => 'first-screen',
	'description' => 'Heading in first screen',
	'before_widget' => null,
	'after_widget' => null
]);





register_post_type('main-article', [
	'labels' => [
		'name'               => 'Article', // основное название для типа записи
		'singular_name'      => 'Artecle', // название для одной записи этого типа
		'add_new'            => 'Add new articel', // для добавления новой записи
		'add_new_item'       => 'Add new article', // заголовка у вновь создаваемой записи в админ-панели.
		'edit_item'          => 'Update the articel', // для редактирования типа записи
		'new_item'           => 'A new article', // текст новой записи
		'view_item'          => 'See the articel', // для просмотра записи этого типа.
		'search_items'       => 'Search', // для поиска по этим типам записи
		'not_found'          => 'Nothing found', // если в результате поиска ничего не было найдено
		'not_found_in_trash' => 'Nothing in trash', // если не было найдено в корзине
		'parent_item_colon'  => '', // для родителей (у древовидных типов)
		'menu_name'          => 'Articles', // название меню
	],
	'public'              => true,
	'menu_position'       => 20,
	'menu_icon'           => 'dashicons-media-document', 
	'hierarchical'        => false,
	'supports'            => array('title', 'editor'),
	'has_archive' => false,
]);


register_taxonomy('product-category', array('products'), array(
	'labels'                => array(
		'name'              => 'Product category',
		'singular_name'     => 'Category',
		'search_items'      => 'Find category',
		'all_items'         => 'All categories',
		'view_item '        => 'View category',
		'edit_item'         => 'Edit',
		'update_item'       => 'Update',
		'add_new_item'      => 'Add category',
		'new_item_name'     => 'Add category',
		'menu_name'         => 'All categories',
	),
	'description'           => '',
	'public'                => true,
	'hierarchical'          => true
));
/* Forms */
$name = wp_strip_all_tags($_POST['order-name']);

$id = wp_insert_post( wp_slash([
	'post_title' => 'Order from: '.$name,
	'post_type' => 'orders',
	'post_status' => 'publish',
	'meta_input' => [
		'order-name' => $name,
		'order-phone' => $phone
	]
]) );

update_field('order-name', $name, $id);

























?>