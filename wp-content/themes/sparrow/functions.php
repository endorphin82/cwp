<?php
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );
add_action( 'after_setup_theme', 'setup_theme' );
add_action( 'widgets_init', 'register_my_widgets' );
add_action( 'init', 'init_fn' );


add_shortcode( 'my_shortcode1', 'my_shortcode1_func' );
function my_shortcode1_func() {
	return '<span style="font-weight: 700;">Это шорткод1</span>';
}

function register_my_widgets() {
	register_sidebar( array(
		'name'          => 'Left Sidebar',
		'id'            => "sidebar_left",
		'description'   => 'Drag-and-drop widgets for Left sidebar',
		'class'         => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<h5 class/="widgettitle">',
		'after_title'   => "</h5>\n",
	) );
}

function enqueue_scripts() {
//	add_filter( 'show_admin_bar', '__return_false' ); /*скрыть панель админки*/
	wp_enqueue_style( 'default', get_template_directory_uri() . '/assets/css/default.css', [], wp_get_theme()->get( 'Version' ), 'all' );
	wp_enqueue_style( 'layout', get_template_directory_uri() . '/assets/css/layout.css', [], wp_get_theme()->get( 'Version' ), 'all' );
	wp_enqueue_style( 'fonts', get_template_directory_uri() . '/assets/css/fonts.css', [], wp_get_theme()->get( 'Version' ), 'all' );
	wp_enqueue_style( 'fonts-awesome', get_template_directory_uri() . '/assets/css/font-awesome/css/font-awesome.min.css', [], wp_get_theme()->get( 'Version' ), 'all' );
	wp_enqueue_style( 'woocommerce', get_template_directory_uri() . '/assets/css/woocommerce.css', [], wp_get_theme()->get( 'Version' ), 'all' );
	wp_enqueue_style( 'media-queries', get_template_directory_uri() . '/assets/css/media-queries.css', [], wp_get_theme()->get( 'Version' ), 'all' );
	wp_enqueue_style( 'style', get_stylesheet_uri() );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/modernizr.js', [], wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'init', get_template_directory_uri() . '/assets/js/init.js', [], wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/assets/js/jquery.flexslider.js', [ 'jquery' ], wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'doubletaptogo', get_template_directory_uri() . '/assets/js/doubletaptogo.js', [], wp_get_theme()->get( 'Version' ), true );
}

function setup_theme() {
	add_theme_support( 'custom-logo' );
	register_nav_menu( 'menu-header', 'Menu in header' );
	register_nav_menu( 'menu-footer', 'Menu in footer' );
	add_theme_support( 'title-tag' );

//	add_theme_support( 'post-thumbnails',  array('post', 'about-post')); // whitelist
	add_theme_support( 'post-thumbnails' );  // all types support thumbnails

//custom excerpt_length & excerpt_more
	add_filter( 'excerpt_length', function () {
		global $myExcerptLength;
		if ( $myExcerptLength ) {
			return $myExcerptLength;
		} else {
			return 20; //default value
		}
	} );
	add_filter( 'excerpt_more', function () {
		global $myExcerptMore;
		global $post;

		if ( $myExcerptMore == 1 ) {
			return '<a href="' . get_permalink( $post ) . '"> . . .</a>';
		} else if ( $myExcerptMore == 2 ) {
			return '<a class="more-link" href="' . get_permalink( $post ) . '"> Read More<i class="fa fa-arrow-circle-o-right"></i></a>';
		}
	} );

	// удаляет H2 из шаблона пагинации
	add_filter( 'navigation_markup_template', 'my_navigation_template', 10, 2 );
	function my_navigation_template( $template, $class ) {
		/*
		Вид базового шаблона:
		<nav class="navigation %1$s" role="navigation">
			<h2 class="screen-reader-text">%2$s</h2>
			<div class="nav-links">%3$s</div>
		</nav>
		*/

		return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
	}

	//custom document title separator
	add_filter( 'document_title_separator', function () {
		return ' ^___^ ';
	} );

	// TODO: bug this filter by author
//	add_filter( 'the_content', function ( $content ) {
//		return $content . ' TY';
//	} );

}

function init_fn() {
	register_post_type( 'about-post', [
		'labels'        => [
			'name'               => 'About Post', // основное название для типа записи
			'singular_name'      => 'About Post', // название для одной записи этого типа
			'add_new'            => 'Add new Post', // для добавления новой записи
			'add_new_item'       => 'Add new Post', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Update the Post', // для редактирования типа записи
			'new_item'           => 'A new Post', // текст новой записи
			'view_item'          => 'See the Post', // для просмотра записи этого типа.
			'search_items'       => 'Search', // для поиска по этим типам записи
			'not_found'          => 'Nothing found', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Nothing in trash', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'About Posts', // название меню
		],
		'public'        => true,
		'menu_position' => 20,
		'menu_icon'     => 'dashicons-palmtree',
		'hierarchical'  => false,
		'supports'      => array( 'title', 'thumbnail', 'editor', 'author' ),
		// title','editor','author','thumbnail','excerpt','comments', 'custom-fields'
		'has_archive'   => false,
		'taxonomy'      => [ 'tax_about' ]
	] );

	register_taxonomy( 'tax_about', [ 'about-post' ], [
		'labels'       => [
			'name'          => 'About tax',
			'singular_name' => 'About tax',
			'search_items'  => 'Find About tax',
			'all_items'     => 'All About tax',
			'view_item '    => 'View About tax',
			'edit_item'     => 'Edit',
			'update_item'   => 'Update',
			'add_new_item'  => 'Add About tax',
			'new_item_name' => 'Add About tax',
			'menu_name'     => 'All About tax',
		],
		'description'  => '',
		'public'       => true,
		'hierarchical' => true,
	] );
}

//filtering id
//function my_special_nav_id($id) {
//    return 'nav';
//}
//add_filter( 'nav_menu_item_id', 'my_special_nav_id' );

//filtering classes
//function my_special_nav_class($classes, $item) {
//    foreach ($classes as $key => $class) {
//        if (preg_match('/^menu-item(.*)|^page(.*)/i', $class)) {
//            unset($classes[$key]);
//        }
//    }
//    return $classes;
//}

//add_filter('nav_menu_css_class', 'my_special_nav_class', 10, 2);

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );
	function mytheme_add_woocommerce_support() {
		add_theme_support( 'woocommerce' );
	}
}

/**
 * Change the breadcrumb separator
 */
add_filter( 'woocommerce_breadcrumb_defaults', function ( $defaults ) {
	// Change the breadcrumb delimeter from '/' to '>'
//	$defaults['delimiter'] = ' &gt; ';
	$defaults['delimiter'] = ' &#8194; &rsaquo; &#8194;';

	return $defaults;
} );

// remove sidebar woocommerce on shop page
add_action( 'woocommerce_before_main_content', function () {
	if ( is_shop() ) {
		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
	}
} );

// change links coverage
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 15 );

//remove button add_to_cart
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

// remove sale default position
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 5 );

/**
 * Hook in on activation
 */

/**
 * Define image sizes
 */
function sparrow_woocommerce_image_dimensions() {
	global $pagenow;

	if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' ) {
		return;
	}

	$catalog = array(
		'width'  => '400',    // px
		'height' => '340',    // px
		'crop'   => 1        // true
	);

	$single = array(
		'width'  => '600',    // px
		'height' => '600',    // px
		'crop'   => 1        // true
	);

	$thumbnail = array(
		'width'  => '120',    // px
		'height' => '120',    // px
		'crop'   => 0        // false
	);

	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog );        // Product category thumbs
	update_option( 'shop_single_image_size', $single );        // Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail );    // Image gallery thumbs
}

add_action( 'after_switch_theme', 'sparrow_woocommerce_image_dimensions', 1 );

// flip hover cart
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_open', 5 );
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_title', 5 );
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 6 );

add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 10 );

add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_open', 15 );
add_action( 'woocommerce_after_shop_loop_item', 'sparrow_short_description', 15 );
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 16 );

add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 20 );

function sparrow_short_description() {
	echo the_excerpt() . '<br>';
}

//registration sidebar woocommerce
function true_register_wp_sidebars() {
	/* В боковой колонке - первый сайдбар */
	register_sidebar(
		array(
			'id'            => 'true_side',
			// уникальный id
			'name'          => 'Боковая колонка',
			// название сайдбара
			'description'   => 'Перетащите сюда виджеты, чтобы добавить их в сайдбар.',
			// описание
			'before_widget' => '<div id="%1$s" class="side widget %2$s">',
			// по умолчанию виджеты выводятся <li>-списком
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			// по умолчанию заголовки виджетов в <h2>
			'after_title'   => '</h3>'
		)
	);
}

add_action( 'widgets_init', 'true_register_wp_sidebars' );

// remove breadcrumb single-product
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
// add breadcrumb single-product
add_action( 'sparrow_woocommerce_before_top_content', 'woocommerce_breadcrumb', 20 );

add_action( 'sparrow_woocommerce_before_top_content', 'art_woo_add_custom_fields', 25 );
function art_woo_add_custom_fields() {
	global $product;
	echo '<div class="options_group">';// Группировка полей
	echo '<h2>Магазин</h2>';
	echo '</div>';
}

// content product
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );

//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

// add container for title and price single-product

add_action( 'woocommerce_single_product_summary', function () {
	echo "<div class=\"sparrow-single-product-header\">";
}, 5, 2 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 6 );
add_action( 'woocommerce_single_product_summary', function () {
	echo "<div class=\"sparrow-single-product-header-left\">";
}, 7, 2 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 9 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_single_product_summary', function () {
	echo "</div>";
}, 11, 2 );
add_action( 'woocommerce_single_product_summary', function () {
	echo "</div>";
}, 11, 2 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_show_product_images', 10 );

// custom content

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

add_action( 'woocommerce_after_single_product_summary', 'bbloomer_wc_output_long_description', 10 );

function bbloomer_wc_output_long_description() {
	?>
	<div class="woocommerce-tabs">
		<?php the_content(); ?>
	</div>
	<?php
}
