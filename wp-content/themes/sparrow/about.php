<?php
/*
Template Name: About
*/; ?>

<?php get_header( 'page' ); ?>

<!-- Page Title
================================================== -->
<div id="page-title">

    <div class="row">

        <div class="ten columns centered text-center">
            <h1>About posts<span>.</span></h1>

            <p>Aenean condimentum, lacus sit amet luctus lobortis, dolores et quas molestias excepturi
                enim tellus ultrices elit, amet consequat enim elit noneas sit amet luctu. </p>
        </div>

    </div>

</div> <!-- Page Title End-->

<!-- Content
================================================== -->
<div class="content-outer">

    <div id="page-content" class="row">

        <div id="primary" class="eight columns">

			<?php
			$args  = [
				'numberposts'      => 3,
				'post_type'        => 'about-post',
				'orderby'          => 'ID',
				'order'            => 'ASC',
				'suppress_filters' => true
			];
			$posts = get_posts( $args );
			foreach ( $posts as $post ) {
				setup_postdata( $post );;
				?>
                <article class="row entry">

                    <div class="entry-header">

                        <div class="permalink">
                            <a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a>
                        </div>

                        <div class="ten columns entry-title pull-right">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        </div>

                        <div class="post-thumb">
                            <a href="<?php the_permalink(); ?>" title=""><?php the_post_thumbnail(); ?></a>
                        </div>

                        <div class="two columns post-meta end">
                            <p>
                                <time datetime="2014-01-31" class="post-date"
                                      pubdate=""><?php the_time( 'F jS, Y' ); ?></time>
                                <span class="dauthor">By <?php the_author(); ?></span>
                            </p>
                        </div>

                    </div>

                    <div class="ten columns offset-2 post-content">

                        <p><?php
							$myExcerptLength = 15;
							$myExcerptMore   = 2;
							$exc             = get_the_excerpt();
							echo $exc;
							$myExcerptLength = 0;
							$myExcerptMore   = 1;
							?> </p>
                    </div>

                </article> <!-- Entry End -->

				<?php wp_reset_postdata();
			}; ?>

        </div> <!-- Primary End-->

        <div id="secondary" class="four columns end">
			<?php get_sidebar(); ?>
        </div> <!-- Secondary End-->

    </div>

</div> <!-- Content End-->

<!-- Tweets Section
================================================== -->
<section id="tweets">

    <div class="row">

        <div class="tweeter-icon align-center">
            <i class="fa fa-twitter"></i>
        </div>

        <ul id="twitter" class="align-center">
            <li>
               <span>
               This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
               Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
               <a href="#">http://t.co/CGIrdxIlI3</a>
               </span>
                <b><a href="#">2 Days Ago</a></b>
            </li>
            <!--
            <li>
               <span>
               This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
               Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
               <a href="#">http://t.co/CGIrdxIlI3</a>
               </span>
               <b><a href="#">3 Days Ago</a></b>
            </li>
            -->
        </ul>

        <p class="align-center"><a href="#" class="button">Follow us</a></p>

    </div>

</section> <!-- Tweets Section End-->

<?php get_footer(); ?>
